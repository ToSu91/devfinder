﻿using DevFinder.Models;
using Firebase.Database;
using Firebase.Database.Streaming;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DevFinder.Services
{
    public class ConversationService : IConversationService
    {
        public event Action<Conversation> OnAdd;
        public event Action<Conversation> OnDelete;
        public event Action<Conversation> OnUpdate;

        private FirebaseClient _client;

        private List<Conversation> _conversations = new List<Conversation>();
        public ConversationService()
        {
            _client = DependencyService.Get<IFireBaseDbProvider>().GetClient();
            _client.Child("main/Conversations").AsObservable<Conversation>().Subscribe((ev) => 
            {
                    Conversation conversation = _conversations.FirstOrDefault(m => m.IdConversation == ev.Key);
                    if (ev.EventType == FirebaseEventType.Delete)
                    {
                        OnDelete?.Invoke(conversation);
                    }
                    else if (ev.EventType == FirebaseEventType.InsertOrUpdate)
                    {

                        if (conversation == null)
                        {
                        Conversation newC = new Conversation
                            {
                                IdConversation = ev.Key,
                                IdDev = ev.Object.IdDev,
                                IdEntr = ev.Object.IdEntr,
                                Message = ev.Object.Message,
                                Read = ev.Object.Read,
                                Sender = ev.Object.Sender,
                                //Time = ev.Object.Time,
                            };
                            _conversations.Add(newC);
                            OnAdd?.Invoke(newC);
                        }
                        else
                        {
                            conversation.Message = ev.Object.Message;
                            //conversation.Time = ev.Object.Time;
                            OnUpdate?.Invoke(conversation);
                        }
                    }
                });
        }
        public void Add(Conversation conversation)
        {
            _client.Child("main/Conversations").PostAsync(JsonConvert.SerializeObject(conversation));
            _conversations.Add(conversation);
        }

        public void Delete(string key)
        {
            _client.Child("main/Conversations/" + key).DeleteAsync();
        }

        public async Task<List<Conversation>> GetAllAsync()
        {
            var elems = await _client.Child("main/Conversations").OnceAsync<Conversation>();
            foreach(var item in elems)
            {
                _conversations.Add(new Conversation
                {
                    IdConversation = item.Key,
                    IdDev =item.Object.IdDev,
                    IdEntr=item.Object.IdEntr,
                    Message=item.Object.Message,
                    Read=item.Object.Read,
                    Sender=item.Object.Sender//,
                    //Time=item.Object.Time
                });
            }
            return _conversations;
        }
        public void Update(Conversation conversation)
        {
            _client.Child("main/Conversations/" + conversation.IdConversation).PutAsync(JsonConvert.SerializeObject(conversation));
        }
    }
}
