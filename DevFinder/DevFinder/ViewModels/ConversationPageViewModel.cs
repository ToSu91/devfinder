﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevFinder.ViewModels
{
    public class ConversationPageViewModel : ViewModelBase
    {
        private string _message;

        public string Message
        {
            get { return _message; }
            set { SetValue(ref _message, value); }
        }


    }
}
