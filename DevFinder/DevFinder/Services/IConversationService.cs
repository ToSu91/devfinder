﻿using DevFinder.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevFinder.Services
{
    public interface IConversationService
    {
        Task<List<Conversation>> GetAllAsync();
        void Add(Conversation Conversation);
        void Delete(string key);
        void Update(Conversation Conversation);

        event Action<Conversation> OnAdd;

        event Action<Conversation> OnDelete;

        event Action<Conversation> OnUpdate;
    }
}
