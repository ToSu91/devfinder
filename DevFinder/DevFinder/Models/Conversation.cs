﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevFinder.Models
{
    public class Conversation
    {
        public string IdConversation { get; set; }
        public string IdDev { get; set; }
        public string IdEntr { get; set; }
        public string Message { get; set; }
        public bool Read { get; set; }
        public string Sender { get; set; } 
        public DateTime Time { get; set; }
    }
}