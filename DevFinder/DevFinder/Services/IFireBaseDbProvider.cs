﻿using Firebase.Database;

namespace DevFinder.Services
{
    public interface IFireBaseDbProvider
    {
        FirebaseClient GetClient();
    }
}