﻿using DevFinder.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevFinder.Services
{
    public interface IUserDataService
    {
        Task<List<User>> GetAllAsync();
        void Add(User user);
        void Delete(string key);
        void Update(User user);

        event Action<User> OnAdd;

        event Action<User> OnDelete;

        event Action<User> OnUpdate;
    }
}
