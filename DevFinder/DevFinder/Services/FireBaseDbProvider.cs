﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevFinder.Services
{
    public class FireBaseDbProvider : IFireBaseDbProvider
    {
        public FirebaseClient GetClient()
        {
            return new FirebaseClient("https://devfinder-corentingoo.firebaseio.com/");
        }
    }
}
