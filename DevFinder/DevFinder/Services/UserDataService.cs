﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFinder.Models;
using Firebase.Database;
using Firebase.Database.Streaming;
using Xamarin.Forms;

namespace DevFinder.Services
{
    public class UserDataService : IUserDataService
    {
        public event Action<User> OnAdd;
        public event Action<User> OnDelete;
        public event Action<User> OnUpdate;

        private FirebaseClient _client;

        private List<User> _users = new List<User>();


        public UserDataService()
        {
            _client = DependencyService.Get<IFireBaseDbProvider>().GetClient();
            _client.Child("main/Users").AsObservable<User>().Subscribe((ev) =>
                  {
                      User user = _users.FirstOrDefault(m => m.Id == ev.Key);
                      if (ev.EventType == FirebaseEventType.Delete)
                      {
                          OnDelete?.Invoke(user);
                          _users.Remove(user);
                      }
                      else if (ev.EventType == FirebaseEventType.InsertOrUpdate)
                      {

                          if (user == null)
                          {
                              User newU = new User();
                              _users.Add(newU);
                              OnAdd?.Invoke(newU);

                          }

                          else
                          {
                              user.Id = ev.Key;
                              user.BirthDate = ev.Object.BirthDate;
                              user.DateSubscribe = ev.Object.DateSubscribe;
                              user.IsEntreprise = ev.Object.IsEntreprise;
                              user.Login= ev.Object.Login;
                              user.Name = ev.Object.Name;
                              user.Password = ev.Object.Password;
                              user.Surname = ev.Object.Surname;
                              user.Skills = ev.Object.Skills;
                              OnUpdate?.Invoke(user);
                          }
                      }
                  });
        }


        public void Add(User user)
        {
            throw new NotImplementedException();
            //_client.Child("fsdf").PostAsync();
        }

        public void Delete(string key)
        {
            throw new NotImplementedException();
        }

        public async Task<List<User>> GetAllAsync()
        {
            var users = await _client.Child("main/Users").OnceAsync<User>();
            foreach (var item in users)
            {
                _users.Add(new User()
                {
                   BirthDate = item.Object.BirthDate,
                   DateSubscribe = item.Object.DateSubscribe,
                   Id = item.Object.Id,
                   IsEntreprise = item.Object.IsEntreprise,
                   Login = item.Object.Login,
                   Name = item.Object.Name,
                   Password = item.Object.Password,
                   Skills = item.Object.Skills,
                   Surname=item.Object.Surname
                });
            }
            return _users;
        }

        public void Update(User user)
        {
            throw new NotImplementedException();
        }
    }
}
