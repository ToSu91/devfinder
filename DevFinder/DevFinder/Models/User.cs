﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevFinder.Models
{
    public class User
    {
        public string Id { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime DateSubscribe { get; set; }
        public bool IsEntreprise { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Surname { get; set; }
        public List<string> Skills { get; set; }
    }
}
